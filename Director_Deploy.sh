#!/bin/bash
#       System: Red Hat OpenStack 8 with Director
#       Creation Date: August 18th, 2016
#       Purpose: Automated Deployment of Openstack 8 with tripleo
#
#       Name: /home/root/Director_Deploy.sh
#       Authors: Sam Cross , Rafael Urena , Jamie Fargen - Emergent 360 Prinicpal Consultants
#
#       See RHN Documentation at: 


####### BEGIN SCRIPT #######

# Uncomment below line for debug setting.
# set -xv

# Path

export PATH=/usr/kerberos/sbin:/usr/kerberos/bin:/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin

# Security
umask 022


####### Variables Begin #######


####### Configurable Variables ########
LOGFILE=/var/log/Director_Deploy.log

####### BEGIN FUNCTIONS #######
################
# creates the controller.yaml file
makeContYaml ()
{
for (( i=0; i < ${#lines[@]}; i++))
do
	line=${lines[$i]}
	if [[ `echo $line | awk -F"," '{print $1}' | tr '[:upper:]' '[:lower:]'` == "control" ]]
	then
		controlindex=$i
		break
	fi
done

local list=$(echo ${lines[$controlindex]} | tr "," " ")

#makes sure that the templates and nic-configs directories exists
[ ! -d templates ] && mkdir templates/
[ ! -d templates/nic-configs ] && mkdir templates/nic-configs/

cat > templates/nic-configs/controller.yaml << EOF
heat_template_version: 2015-04-30

description: >
  Software Config to drive os-net-config to configure VLANs for the
  controller role.

parameters:
  ControlPlaneIp:
    default: ''
    description: IP address/subnet on the ctlplane network
    type: string
  ExternalIpSubnet:
    default: ''
    description: IP address/subnet on the external network
    type: string
  InternalApiIpSubnet:
    default: ''
    description: IP address/subnet on the internal API network
    type: string
  StorageIpSubnet:
    default: ''
    description: IP address/subnet on the storage network
    type: string
  StorageMgmtIpSubnet:
    default: ''
    description: IP address/subnet on the storage mgmt network
    type: string
  TenantIpSubnet:
    default: ''
    description: IP address/subnet on the tenant network
    type: string
  ManagementIpSubnet: # Only populated when including environments/network-management.yaml
    default: ''
    description: IP address/subnet on the management network
    type: string
EOF

#finds the end of the section
for (( i=$vlanindex ; i < ${#lines[@]} ; i++ ))
do
	end=$i
	[ -z `echo ${lines[$i+1]} | awk -F"," '{print $2}' | tr -d '[[:space:]]'` ] && break

done

# cycles through the line called list and adds the parameters needed for the desired vlans
for i in $list
do
	
	if [[ i == "control" ]]
	then 
		continue
	fi

	for (( l=$vlanindex ; l<=$end ; l++ ))
	do
		j=${lines[$l]}
		if [[ `echo $j | awk -F"," '{print $1}'` == $i ]]
		then
			case `echo $j | awk -F"," '{print $2}' | tr '[:upper:]' '[:lower:]'` in
			external)
			echo "  ExternalNetworkVlanID:" >> templates/nic-configs/controller.yaml
			echo "    default: $i" >> templates/nic-configs/controller.yaml
			echo "    description: Vlan ID for the external network traffic." >> templates/nic-configs/controller.yaml
			echo "    type: number" >> templates/nic-configs/controller.yaml
			echo "  ExternalInterfaceDefaultRoute:" >> templates/nic-configs/controller.yaml
			echo "    default: '`echo $j | awk -F"," '{print $3}'`'" >> templates/nic-configs/controller.yaml
			echo "    description: default route for the external network" >> templates/nic-configs/controller.yaml
			echo "    type: string" >> templates/nic-configs/controller.yaml
			;;
			internal)
			echo "  InternalApiNetworkVlanID:" >> templates/nic-configs/controller.yaml
			echo "    default: $i" >> templates/nic-configs/controller.yaml
			echo "    description: Vlan ID for the internal API network traffic." >> templates/nic-configs/controller.yaml
			echo "    type: number" >> templates/nic-configs/controller.yaml
			;;
			storage)
			echo "  StorageNetworkVlanID:" >> templates/nic-configs/controller.yaml
			echo "    default: $i" >> templates/nic-configs/controller.yaml
			echo "    description: Vlan ID for the storage network traffic." >> templates/nic-configs/controller.yaml
			echo "    type: number" >> templates/nic-configs/controller.yaml
			;;
			storagemgmt)
			echo "  StorageMgmtNetworkVlanID:" >> templates/nic-configs/controller.yaml
			echo "    default: $i" >> templates/nic-configs/controller.yaml
			echo "    description: Vlan ID for the storage management network traffic." >> templates/nic-configs/controller.yaml
			echo "    type: number" >> templates/nic-configs/controller.yaml
			;;
			tenant)
			echo "  TenantNetworkVlanID:" >> templates/nic-configs/controller.yaml
			echo "    default: $i" >> templates/nic-configs/controller.yaml
			echo "    description: Vlan ID for the tenant network traffic." >> templates/nic-configs/controller.yaml
			echo "    type: number" >> templates/nic-configs/controller.yaml
			;;
			management)
			echo "  ManangementNetworkVlanID:" >> templates/nic-configs/controller.yaml
			echo "    default: $i" >> templates/nic-configs/controller.yaml
			echo "    description: Vlan ID for the management network traffic." >> templates/nic-configs/controller.yaml
			echo "    type: number" >> templates/nic-configs/controller.yaml
			;;
			esac
		fi
	done	
done
cat >> templates/nic-configs/controller.yaml <<EOF
  ControlPlaneSubnetCidr: # Override this via parameter_defaults
    default: '24'
    description: The subnet CIDR of the control plane network.
    type: string
  DnsServers: # Override this via parameter_defaults
    default: []
    description: A list of DNS servers (2 max for some implementations) that will be added to resolv.conf.
    type: comma_delimited_list
  EC2MetadataIp: # Override this via parameter_defaults
    description: The IP address of the EC2 metadata server.
    type: string

resources:
  OsNetConfigImpl:
    type: OS::Heat::StructuredConfig
    properties:
      group: os-apply-config
      config:
        os_net_config:
          network_config:
            -
              type: interface
EOF
echo "              name: $opronet" >> templates/nic-configs/controller.yaml
cat >> templates/nic-configs/controller.yaml << EOF
              use_dhcp: false
              dns_servers: {get_param: DnsServers}
              addresses:
                - ip_netmask: {list_join: ['/', [{get_param: ControlPlaneIp}, {get_param: ControlPlaneSubnetCidr}]]}
              routes:
                - ip_netmask: 169.254.169.254/32
                  next_hop: {get_param: EC2MetadataIp}
            -
              type: ovs_bridge
              name: {get_input: bridge_name}
              use_dhcp: false
              members:
                -
                  type: interface
EOF
echo "                  name: $ovlannet" >> templates/nic-configs/controller.yaml
cat >> templates/nic-configs/controller.yaml << EOF 
                  # force the MAC address of the bridge to this interface
                  primary: true
EOF

# cycles through the line called list and adds the parameters needed for the desired vlans to the vlan network settings
for i in $list
do	
	if [[ i == "control" ]]
	then 
		continue
	fi

	for (( l=$vlanindex ; l<=$end; l++ ))
	do
		j=${lines[$l]}
		if [[ `echo $j | awk -F"," '{print $1}'` == $i ]]
		then
			case `echo $j | awk -F"," '{print $2}' | tr '[:upper:]' '[:lower:]'` in
			external)
				echo "                -" >> templates/nic-configs/controller.yaml
				echo "                  type: vlan" >> templates/nic-configs/controller.yaml
				echo "                  vlan_id: {get_param: ExternalNetworkVlanID}" >> templates/nic-configs/controller.yaml
				echo "                  addresses:" >> templates/nic-configs/controller.yaml
				echo "                  -" >> templates/nic-configs/controller.yaml
				echo "                    ip_netmask: {get_param: ExternalIpSubnet}" >> templates/nic-configs/controller.yaml
				echo "                  routes:" >> templates/nic-configs/controller.yaml
				echo "                    -" >> templates/nic-configs/controller.yaml
				echo "                      default: true" >> templates/nic-configs/controller.yaml
				echo "                      next_hop: {get_param: ExternalInterfaceDefaultRoute}" >> templates/nic-configs/controller.yaml
				;;
			internal)
				echo "                -" >> templates/nic-configs/controller.yaml
				echo "                  type: vlan" >> templates/nic-configs/controller.yaml
				echo "                  vlan_id: {get_param: InternalApiNetworkVlanID}" >> templates/nic-configs/controller.yaml
				echo "                  addresses:" >> templates/nic-configs/controller.yaml
				echo "                    -" >> templates/nic-configs/controller.yaml
				echo "                      ip_netmask: {get_param: InternalApiIpSubnet}" >> templates/nic-configs/controller.yaml
				;;
			storage)
				echo "                -" >> templates/nic-configs/controller.yaml
				echo "                  type: vlan" >> templates/nic-configs/controller.yaml
				echo "                  vlan_id: {get_param: StorageNetworkVlanID}" >> templates/nic-configs/controller.yaml
				echo "                  addresses:" >> templates/nic-configs/controller.yaml
				echo "                    -" >> templates/nic-configs/controller.yaml
				echo "                      ip_netmask: {get_param: StorageIpSubnet}" >> templates/nic-configs/controller.yaml
				;;
			storagemgmt)
				echo "                -" >> templates/nic-configs/controller.yaml
				echo "                  type: vlan" >> templates/nic-configs/controller.yaml
				echo "                  vlan_id: {get_param: StorageMgmtNetworkVlanID}" >> templates/nic-configs/controller.yaml
				echo "                  addresses:" >> templates/nic-configs/controller.yaml
				echo "                    -" >> templates/nic-configs/controller.yaml
				echo "                      ip_netmask: {get_param: StorageMgmtIpSubnet}" >> templates/nic-configs/controller.yaml
				;;
			tenant)
				echo "                -" >> templates/nic-configs/controller.yaml
				echo "                  type: vlan" >> templates/nic-configs/controller.yaml
				echo "                  vlan_id: {get_param: TenantNetworkVlanID}" >> templates/nic-configs/controller.yaml
				echo "                  addresses:" >> templates/nic-configs/controller.yaml
				echo "                    -" >> templates/nic-configs/controller.yaml
				echo "                      ip_netmask: {get_param: TenantIpSubnet}" >> templates/nic-configs/controller.yaml
				;;
			management)
				echo "                -" >> templates/nic-configs/controller.yaml
				echo "                  type: vlan" >> templates/nic-configs/controller.yaml
				echo "                  vlan_id: {get_param: ManagementNetworkVlanID}" >> templates/nic-configs/controller.yaml
				echo "                  addresses:" >> templates/nic-configs/controller.yaml
				echo "                    -" >> templates/nic-configs/controller.yaml
				echo "                      ip_netmask: {get_param: ManagementIpSubnet}" >> templates/nic-configs/controller.yaml
				;;
			esac

		fi
	done
done
cat >> templates/nic-configs/controller.yaml << EOF

outputs:
  OS::stack_id:
    description: The OsNetConfigImpl resource.
    value: {get_resource: OsNetConfigImpl}
EOF
}

################
# Creates the Compute.yaml file
makeCompYaml ()
{

# Finds the compute line in the csv and saves the index
for (( i=0; i < ${#lines[@]}; i++))
do
	line=${lines[$i]}
	if [[ `echo $line | awk -F"," '{print $1}' | tr '[:upper:]' '[:lower:]'` == "compute" ]]
	then
		computeindex=$i
		break
	fi
done

#puts that line into variable list and replaces the commas with spaces
local list=$(echo ${lines[$computeindex]} | tr "," " ")

#makes sure that the templates and nic-configs directories exists
[ ! -d templates ] && mkdir templates/
[ ! -d templates/nic-configs ] && mkdir templates/nic-configs/

cat > templates/nic-configs/compute.yaml << EOF
heat_template_version: 2015-04-30

description: >
  Software Config to drive os-net-config to configure VLANs for the compute role.

parameters: 
  ControlPlaneIp:
    default: '' 
    description: IP address/subnet on the ctlplane network
    type: string
  ExternalIpSubnet: 
    default: '' 
    description: IP address/subnet on the external network
    type: string
  InternalApiIpSubnet:
    default: ''
    description: IP address/subnet on the internal API network 
    type: string
  StorageIpSubnet:
    default: ''
    description: IP address/subnet on the storage network 
    type: string
  StorageMgmtIpSubnet:
    default: ''
    description: IP address/subnet on the storage mgmt network 
    type: string
  TenantIpSubnet:
    default: ''
    description: IP address/subnet on the tenant network 
    type: string
  ManagementIpSubnet: # Only populated when including environments/network-management.yaml
    default: ''
    description: IP address/subnet on the management network 
    type: string
EOF

#finds the end of the section
for (( i=$vlanindex ; i < ${#lines[@]} ; i++ ))
do
	end=$i
	[ -z `echo ${lines[$i+1]} | awk -F"," '{print $2}' | tr -d '[[:space:]]'` ] && break

done

# Adds the necessary vlans
for i in $list
do
	if [[ i == "compute" ]]
	then 
		continue
	fi
	for (( l=$vlanindex ; l<=$end; l++ ))
	do
		j=${lines[$l]}
		if [[ `echo $j | awk -F"," '{print $1}'` == $i ]]
		then
			case `echo $j | awk -F"," '{print $2}' | tr '[:upper:]' '[:lower:]'` in
			external)
			echo "  ExternalNetworkVlanID:" >> templates/nic-configs/compute.yaml
			echo "    default: $i" >> templates/nic-configs/compute.yaml
			echo "    description: Vlan ID for the external network traffic." >> templates/nic-configs/compute.yaml
			echo "    type: number" >> templates/nic-configs/compute.yaml
			echo "  ExternalInterfaceDefaultRoute:" >> templates/nic-configs/compute.yaml
			echo "    default: '`echo $j | awk -F"," '{print $3}'`'" >> templates/nic-configs/compute.yaml
			echo "    description: default route for the external network" >> templates/nic-configs/compute.yaml
			echo "    type: string" >> templates/nic-configs/compute.yaml
			;;
			internal)
			echo "  InternalApiNetworkVlanID:" >> templates/nic-configs/compute.yaml
			echo "    default: $i" >> templates/nic-configs/compute.yaml
			echo "    description: Vlan ID for the internal API network traffic." >> templates/nic-configs/compute.yaml
			echo "    type: number" >> templates/nic-configs/compute.yaml
			;;
			storage)
			echo "  StorageNetworkVlanID:" >> templates/nic-configs/compute.yaml
			echo "    default: $i" >> templates/nic-configs/compute.yaml
			echo "    description: Vlan ID for the storage network traffic." >> templates/nic-configs/compute.yaml
			echo "    type: number" >> templates/nic-configs/compute.yaml
			;;
			storagemgmt)
			echo "  StorageMgmtNetworkVlanID:" >> templates/nic-configs/compute.yaml
			echo "    default: $i" >> templates/nic-configs/compute.yaml
			echo "    description: Vlan ID for the storage management network traffic." >> templates/nic-configs/compute.yaml
			echo "    type: number" >> templates/nic-configs/compute.yaml
			;;
			tenant)
			echo "  TenantNetworkVlanID:" >> templates/nic-configs/compute.yaml
			echo "    default: $i" >> templates/nic-configs/compute.yaml
			echo "    description: Vlan ID for the tenant network traffic." >> templates/nic-configs/compute.yaml
			echo "    type: number" >> templates/nic-configs/compute.yaml
			;;
			management)
			echo "  ManangementNetworkVlanID:" >> templates/nic-configs/compute.yaml
			echo "    default: $i" >> templates/nic-configs/compute.yaml
			echo "    description: Vlan ID for the management network traffic." >> templates/nic-configs/compute.yaml
			echo "    type: number" >> templates/nic-configs/compute.yaml
			;;
			esac
		fi
	done	
done

cat >> templates/nic-configs/compute.yaml <<EOF
  ControlPlaneSubnetCidr: # Override this via parameter_defaults
    default: '24'
    description: The subnet CIDR of the control plane network.
    type: string
  DnsServers: # Override this via parameter_defaults
    default: []
    description: A list of DNS servers (2 max for some implementations) that will be added to resolv.conf.
    type: comma_delimited_list
  EC2MetadataIp: # Override this via parameter_defaults
    description: The IP address of the EC2 metadata server.
    type: string

resources:
  OsNetConfigImpl:
    type: OS::Heat::StructuredConfig
    properties:
      group: os-apply-config
      config:
        os_net_config:
          network_config:
            -
              type: interface
EOF
echo "              name: $opronet" >> templates/nic-configs/compute.yaml
cat >> templates/nic-configs/compute.yaml << EOF
              use_dhcp: false
              dns_servers: {get_param: DnsServers}
              addresses:
                - ip_netmask: {list_join: ['/', [{get_param: ControlPlaneIp}, {get_param: ControlPlaneSubnetCidr}]]}
              routes:
                - ip_netmask: 169.254.169.254/32
                  next_hop: {get_param: EC2MetadataIp}
            -
              type: ovs_bridge
              name: {get_input: bridge_name}
              use_dhcp: false
              members:
                -
                  type: interface
EOF
echo "                  name: $ovlannet" >> templates/nic-configs/compute.yaml
cat >> templates/nic-configs/compute.yaml << EOF
                  # force the MAC address of the bridge to this interface
                  primary: true
EOF

# Cycles through the list and adds the necessary vlans to the vlan port
for i in $list
do
	if [[ i == "compute" ]]
	then 
		continue
	fi
	for (( l=$vlanindex ; l<=$end; l++ ))
	do
		j=${lines[$l]}
		if [[ `echo $j | awk -F"," '{print $1}'` == $i ]]
		then
			case `echo $j | awk -F"," '{print $2}' | tr '[:upper:]' '[:lower:]'` in
			external)
				echo "                -" >> templates/nic-configs/compute.yaml
				echo "                  type: vlan" >> templates/nic-configs/compute.yaml
				echo "                  vlan_id: {get_param: ExternalNetworkVlanID}" >> templates/nic-configs/compute.yaml
				echo "                  addresses:" >> templates/nic-configs/compute.yaml
				echo "                  -" >> templates/nic-configs/compute.yaml
				echo "                    ip_netmask: {get_param: ExternalIpSubnet}" >> templates/nic-configs/compute.yaml
				echo "                  routes:" >> templates/nic-configs/compute.yaml
				echo "                    -" >> templates/nic-configs/compute.yaml
				echo "                      default: true" >> templates/nic-configs/compute.yaml
				echo "                      next_hop: {get_param: ExternalInterfaceDefaultRoute}" >> templates/nic-configs/compute.yaml
				;;
			internal)
				echo "                -" >> templates/nic-configs/compute.yaml
				echo "                  type: vlan" >> templates/nic-configs/compute.yaml
				echo "                  vlan_id: {get_param: InternalApiNetworkVlanID}" >> templates/nic-configs/compute.yaml
				echo "                  addresses:" >> templates/nic-configs/compute.yaml
				echo "                    -" >> templates/nic-configs/compute.yaml
				echo "                      ip_netmask: {get_param: InternalApiIpSubnet}" >> templates/nic-configs/compute.yaml
				;;
			storage)
				echo "                -" >> templates/nic-configs/compute.yaml
				echo "                  type: vlan" >> templates/nic-configs/compute.yaml
				echo "                  vlan_id: {get_param: StorageNetworkVlanID}" >> templates/nic-configs/compute.yaml
				echo "                  addresses:" >> templates/nic-configs/compute.yaml
				echo "                    -" >> templates/nic-configs/compute.yaml
				echo "                      ip_netmask: {get_param: StorageIpSubnet}" >> templates/nic-configs/compute.yaml
				;;
			storagemgmt)
				echo "                -" >> templates/nic-configs/compute.yaml
				echo "                  type: vlan" >> templates/nic-configs/compute.yaml
				echo "                  vlan_id: {get_param: StorageMgmtNetworkVlanID}" >> templates/nic-configs/compute.yaml
				echo "                  addresses:" >> templates/nic-configs/compute.yaml
				echo "                    -" >> templates/nic-configs/compute.yaml
				echo "                      ip_netmask: {get_param: StorageMgmtIpSubnet}" >> templates/nic-configs/compute.yaml
				;;
			tenant)
				echo "                -" >> templates/nic-configs/compute.yaml
				echo "                  type: vlan" >> templates/nic-configs/compute.yaml
				echo "                  vlan_id: {get_param: TenantNetworkVlanID}" >> templates/nic-configs/compute.yaml
				echo "                  addresses:" >> templates/nic-configs/compute.yaml
				echo "                    -" >> templates/nic-configs/compute.yaml
				echo "                      ip_netmask: {get_param: TenantIpSubnet}" >> templates/nic-configs/compute.yaml
				;;
			management)
				echo "                -" >> templates/nic-configs/compute.yaml
				echo "                  type: vlan" >> templates/nic-configs/compute.yaml
				echo "                  vlan_id: {get_param: ManagementNetworkVlanID}" >> templates/nic-configs/compute.yaml
				echo "                  addresses:" >> templates/nic-configs/compute.yaml
				echo "                    -" >> templates/nic-configs/compute.yaml
				echo "                      ip_netmask: {get_param: ManagementIpSubnet}" >> templates/nic-configs/compute.yaml
				;;
				esac
		fi
	done
done

cat >> templates/nic-configs/compute.yaml << EOF

outputs:
  OS::stack_id:
    description: The OsNetConfigImpl resource.
    value: {get_resource: OsNetConfigImpl}
EOF

}

################
# Creates the ceph-storage.yaml file
makeCephYaml ()
{
# gets the index for the ceph line in the csv file
for (( i=0; i < ${#lines[@]}; i++))
do
	line=${lines[$i]}
	if [[ `echo $line | awk -F"," '{print $1}' | tr '[:upper:]' '[:lower:]'` == "ceph" ]]
	then
		cephindex=$i
		break
	fi
done

# loads the line and replaces the commas with spaces
local list=$(echo ${lines[$cephindex]} | tr "," " ")

# makes sure that the templates and nic-configs directories exists
[ ! -d templates ] && mkdir templates/
[ ! -d templates/nic-configs ] && mkdir templates/nic-configs/

cat > templates/nic-configs/ceph-storage.yaml << EOF
heat_template_version: 2015-04-30

description: >
  Software Config to drive os-net-config to configure VLANs for the
  ceph storage role.

parameters:
  ControlPlaneIp:
    default: ''
    description: IP address/subnet on the ctlplane network
    type: string
  ExternalIpSubnet:
    default: ''
    description: IP address/subnet on the external network
    type: string
  InternalApiIpSubnet:
    default: ''
    description: IP address/subnet on the internal API network
    type: string
  StorageIpSubnet:
    default: ''
    description: IP address/subnet on the storage network
    type: string
  StorageMgmtIpSubnet:
    default: ''
    description: IP address/subnet on the storage mgmt network
    type: string
  TenantIpSubnet:
    default: ''
    description: IP address/subnet on the tenant network
    type: string
  ManagementIpSubnet: # Only populated when including environments/network-management.yaml
    default: ''
    description: IP address/subnet on the management network
    type: string
EOF

#finds the end of the section
for (( i=$vlanindex ; i < ${#lines[@]} ; i++ ))
do
	end=$i
	[ -z `echo ${lines[$i+1]} | awk -F"," '{print $2}' | tr -d '[[:space:]]'` ] && break
done

# Cycles through the list and adds the vlan info to the file
for i in $list
do
	if [[ i == "ceph-storage" ]]
	then 
		continue
	fi
	for (( l=$vlanindex ; l<=$end; ++ l ))
	do
		j=${lines[$l]}
		if [[ `echo $j | awk -F"," '{print $1}'` == $i ]]
		then
			case `echo $j | awk -F"," '{print $2}' | tr '[:upper:]' '[:lower:]'` in
			external)
			echo "  ExternalNetworkVlanID:" >> templates/nic-configs/ceph-storage.yaml
			echo "    default: $i" >> templates/nic-configs/ceph-storage.yaml
			echo "    description: Vlan ID for the external network traffic." >> templates/nic-configs/ceph-storage.yaml
			echo "    type: number" >> templates/nic-configs/ceph-storage.yaml
			echo "  ExternalInterfaceDefaultRoute:" >> templates/nic-configs/ceph-storage.yaml
			echo "    default: '`echo $j | awk -F"," '{print $3}'`'" >> templates/nic-configs/ceph-storage.yaml
			echo "    description: default route for the external network" >> templates/nic-configs/ceph-storage.yaml
			echo "    type: string" >> templates/nic-configs/ceph-storage.yaml
			;;
			internal)
			echo "  InternalApiNetworkVlanID:" >> templates/nic-configs/ceph-storage.yaml
			echo "    default: $i" >> templates/nic-configs/ceph-storage.yaml
			echo "    description: Vlan ID for the internal API network traffic." >> templates/nic-configs/ceph-storage.yaml
			echo "    type: number" >> templates/nic-configs/ceph-storage.yaml
			;;
			storage)
			echo "  StorageNetworkVlanID:" >> templates/nic-configs/ceph-storage.yaml
			echo "    default: $i" >> templates/nic-configs/ceph-storage.yaml
			echo "    description: Vlan ID for the storage network traffic." >> templates/nic-configs/ceph-storage.yaml
			echo "    type: number" >> templates/nic-configs/ceph-storage.yaml
			;;
			storagemgmt)
			echo "  StorageMgmtNetworkVlanID:" >> templates/nic-configs/ceph-storage.yaml
			echo "    default: $i" >> templates/nic-configs/ceph-storage.yaml
			echo "    description: Vlan ID for the storage management network traffic." >> templates/nic-configs/ceph-storage.yaml
			echo "    type: number" >> templates/nic-configs/ceph-storage.yaml
			;;
			tenant)
			echo "  TenantNetworkVlanID:" >> templates/nic-configs/ceph-storage.yaml
			echo "    default: $i" >> templates/nic-configs/ceph-storage.yaml
			echo "    description: Vlan ID for the tenant network traffic." >> templates/nic-configs/ceph-storage.yaml
			echo "    type: number" >> templates/nic-configs/ceph-storage.yaml
			;;
			management)
			echo "  ManangementNetworkVlanID:" >> templates/nic-configs/ceph-storage.yaml
			echo "    default: $i" >> templates/nic-configs/ceph-storage.yaml
			echo "    description: Vlan ID for the management network traffic." >> templates/nic-configs/ceph-storage.yaml
			echo "    type: number" >> templates/nic-configs/ceph-storage.yaml
			;;
			esac
		fi
	done	
done

cat >> templates/nic-configs/ceph-storage.yaml <<EOF
  ControlPlaneSubnetCidr: # Override this via parameter_defaults
    default: '24'
    description: The subnet CIDR of the control plane network.
    type: string
  DnsServers: # Override this via parameter_defaults
    default: []
    description: A list of DNS servers (2 max for some implementations) that will be added to resolv.conf.
    type: comma_delimited_list
  EC2MetadataIp: # Override this via parameter_defaults
    description: The IP address of the EC2 metadata server.
    type: string

resources:
  OsNetConfigImpl:
    type: OS::Heat::StructuredConfig
    properties:
      group: os-apply-config
      config:
        os_net_config:
          network_config:
            -
              type: interface
EOF

echo "              name: $opronet" >> templates/nic-configs/ceph-storage.yaml

cat >> templates/nic-configs/ceph-storage.yaml << EOF
              use_dhcp: false
              dns_servers: {get_param: DnsServers}
              addresses:
                - ip_netmask: {list_join: ['/', [{get_param: ControlPlaneIp}, {get_param: ControlPlaneSubnetCidr}]]}
              routes:
                - ip_netmask: 169.254.169.254/32
                  next_hop: {get_param: EC2MetadataIp}
            -
              type: ovs_bridge
              name: {get_input: bridge_name}
              use_dhcp: false
              members:
                -
                  type: interface
EOF

echo "                  name: $ovlannet" >> templates/nic-configs/ceph-storage.yaml

cat >> templates/nic-configs/ceph-storage.yaml << EOF 
                  # force the MAC address of the bridge to this interface
                  primary: true
EOF

# Cycles through the list and adds the required parameters to the network port for the vlans
for i in $list
do
	if [[ i == "ceph-storage" ]]
	then 
		continue
	fi
	for (( l=$vlanindex ; l<=$end ; l++ ))
	do
		j=${lines[$l]}
		if [[ `echo $j | awk -F"," '{print $1}'` == $i ]]
		then
			case `echo $j | awk -F"," '{print $2}' | tr '[:upper:]' '[:lower:]'` in
			external)
				echo "                -" >> templates/nic-configs/ceph-storage.yaml
				echo "                  type: vlan" >> templates/nic-configs/ceph-storage.yaml
				echo "                  vlan_id: {get_param: ExternalNetworkVlanID}" >> templates/nic-configs/ceph-storage.yaml
				echo "                  addresses:" >> templates/nic-configs/ceph-storage.yaml
				echo "                  -" >> templates/nic-configs/ceph-storage.yaml
				echo "                    ip_netmask: {get_param: ExternalIpSubnet}" >> templates/nic-configs/ceph-storage.yaml
				echo "                  routes:" >> templates/nic-configs/ceph-storage.yaml
				echo "                    -" >> templates/nic-configs/ceph-storage.yaml
				echo "                      default: true" >> templates/nic-configs/ceph-storage.yaml
				echo "                      next_hop: {get_param: ExternalInterfaceDefaultRoute}" >> templates/nic-configs/ceph-storage.yaml
				;;
			internal)
				echo "                -" >> templates/nic-configs/ceph-storage.yaml
				echo "                  type: vlan" >> templates/nic-configs/ceph-storage.yaml
				echo "                  vlan_id: {get_param: InternalApiNetworkVlanID}" >> templates/nic-configs/ceph-storage.yaml
				echo "                  addresses:" >> templates/nic-configs/ceph-storage.yaml
				echo "                    -" >> templates/nic-configs/ceph-storage.yaml
				echo "                      ip_netmask: {get_param: InternalApiIpSubnet}" >> templates/nic-configs/ceph-storage.yaml
				;;  
			storage)
				echo "                -" >> templates/nic-configs/ceph-storage.yaml
				echo "                  type: vlan" >> templates/nic-configs/ceph-storage.yaml
				echo "                  vlan_id: {get_param: StorageNetworkVlanID}" >> templates/nic-configs/ceph-storage.yaml
				echo "                  addresses:" >> templates/nic-configs/ceph-storage.yaml
				echo "                    -" >> templates/nic-configs/ceph-storage.yaml
				echo "                      ip_netmask: {get_param: StorageIpSubnet}" >> templates/nic-configs/ceph-storage.yaml
				;;
			storagemgmt)
				echo "                -" >> templates/nic-configs/ceph-storage.yaml
				echo "                  type: vlan" >> templates/nic-configs/ceph-storage.yaml
				echo "                  vlan_id: {get_param: StorageMgmtNetworkVlanID}" >> templates/nic-configs/ceph-storage.yaml
				echo "                  addresses:" >> templates/nic-configs/ceph-storage.yaml
				echo "                  -" >> templates/nic-configs/ceph-storage.yaml
				echo "                    ip_netmask: {get_param: StorageMgmtIpSubnet}" >> templates/nic-configs/ceph-storage.yaml
				;;
			tenant)
				echo "                -" >> templates/nic-configs/ceph-storage.yaml
				echo "                  type: vlan" >> templates/nic-configs/ceph-storage.yaml
				echo "                  vlan_id: {get_param: TenantNetworkVlanID}" >> templates/nic-configs/ceph-storage.yaml
				echo "                  addresses:" >> templates/nic-configs/ceph-storage.yaml
				echo "                    -" >> templates/nic-configs/ceph-storage.yaml
				echo "                      ip_netmask: {get_param: TenantIpSubnet}" >> templates/nic-configs/ceph-storage.yaml
				;;
			management)
				echo "                -" >> templates/nic-configs/ceph-storage.yaml
				echo "                  type: vlan" >> templates/nic-configs/ceph-storage.yaml
				echo "                  vlan_id: {get_param: ManagementNetworkVlanID}" >> templates/nic-configs/ceph-storage.yaml
				echo "                  addresses:" >> templates/nic-configs/ceph-storage.yaml
				echo "                    -" >> templates/nic-configs/ceph-storage.yaml
				echo "                      ip_netmask: {get_param: ManagementIpSubnet}" >> templates/nic-configs/ceph-storage.yaml
				;;
				esac
		fi
	done
done

cat >> templates/nic-configs/ceph-storage.yaml << EOF

outputs:
  OS::stack_id:
    description: The OsNetConfigImpl resource.
    value: {get_resource: OsNetConfigImpl}

EOF

}

######################
# makes files to prepare the ceph nodes
storage ()
{
# Finds the indes to ceph-storage
for (( i=0; i < ${#lines[@]}; i++))
do
	line=${lines[$i]}
	if [[ `echo $line | awk -F"," '{print $1}' | tr '[:upper:]' '[:lower:]'` == "ceph-storage" ]]
	then
		if [[ -z `echo $line | awk -F"," '{print $2}'` ]]
		then
			index=$i
			break
		fi
	fi
done

#finds the end of the section
for (( i=$index ; i < ${#lines[@]} ; i++ ))
do
	end=$i
	[ -z `echo ${lines[$i+1]} | awk -F"," '{print $2}' | tr -d '[[:space:]]'` ] && break
done

#makes sure that the templates and firstboot directories exists
[ ! -d templates ] && mkdir templates/
[ ! -d templates/firstboot ] && mkdir templates/firstboot/

# This yaml will setup the drives so that they can be used as osds
cat > templates/firstboot/wipe-disks.yaml << 'EOF'
heat_template_version: 2014-10-16

description: >
 Wipe and convert all disks to GPT (except the disk containing the root file system)

resources:
 userdata:
   type: OS::Heat::MultipartMime
   properties:
     parts:
     - config: {get_resource: wipe_disk}

 wipe_disk:
 type: OS::Heat::SoftwareConfig
 properties:
 config: |
   #!/bin/bash
   /bin/hostnamectl | grep 'hostname' | grep ceph -q ;
   if [ $? == 0 ]; then
   for i in {b}; do dd if=/dev/zero of /dev/sd$ bs=1024k count=100 && sgdisk -g /dev/sd$; done
   fi

outputs:
 OS::stack_id:
   value: {get_resource: userdata}

EOF

# Storage Environmental file for setup for ceph
cat > templates/storage-environment.yaml << EOF
resource_registry:
  OS::TripleO::NodeUserData: /home/stack/templates/firstboot/wipe-disks.yaml
parameter_defaults:
  CinderEnableIscsiBackend: false
  CinderEnableRbdBackend: true
  CinderEnableNfsBackend: false
  NovaEnableRbdBackend: true
  GlanceBackend: rbd
  # CinderNfsMountOptions: 'rw,sync'
  # CinderNfsServers: '192.0.2.40:/exports/cinder'
  # GlanceFilePcmkManage: true
  # GlanceFilePcmkFstype: nfs
  # GlanceFilePcmkDevice: '192.168.122.1:/exports/glance'
  # GlanceFilePcmkOptions: 'rw,sync,context=system_u:object_r:glance_var_lib_t:s0'
  # ControllerEnableCephStorage: false
  # CephStorageCount: 0
  # CephClusterFSID: ''
  # CephMonKey: ''
  # CephAdminKey: ''
  ExtraConfig:
    ceph::profile::params::osds:
EOF

# this will add the settings needed for the ceph hard drives. 
# if SSD and Journal that drive will be designated the journal drive. this should be the first on the list.
# if not SSD and journal then 2 lines are added
# if SSD and not journaled then 1 line is added
# if not ssd and not journal then 1 line is added

for (( i=$index ; i<=$end; i++ ))
do
	local line=${lines[$i]}
	if [[ `echo $line | awk -F"," '{print $1}' | tr '[:upper:]' '[:lower:]'`  == "osds" ]]
	then
		continue
	fi

	if [[ "`echo $line | awk -F"," '{print $2}'`" == "y" ]] && [[ "`echo $line | awk -F"," '{print $3}'`" == "y" ]]
	then
		journal=`echo $line | awk -F"," '{print $1}'`
	fi

	if [[ "`echo $line | awk -F"," '{print $2}'`" == "n" ]] && [[ "`echo $line | awk -F"," '{print $3}'`" == "y" ]]
	then
		[ -z $journal ] && echo "      '/dev/`echo $line | awk -F"," '{print $1}'`':" >> templates/storage-environment.yaml
		[ -z $journal ] && echo "          journal: '/dev/$journal'" >> templates/storage-environment.yaml
	fi

	if [[ "`echo $line | awk -F"," '{print $2}'`" == "y" ]] && [[ "`echo $line | awk -F"," '{print $3}'`" == "n" ]]
	then
		echo "      '/dev/`echo $line | awk -F"," '{print $1}'`': {}" >> templates/storage-environment.yaml
	fi

	if [[ "`echo $line | awk -F"," '{print $2}'`" == "n" ]] && [[ "`echo $line | awk -F"," '{print $3}'`" == "n" ]]
	then
		echo "      '/dev/`echo $line | awk -F"," '{print $1}'`': {}" >> templates/storage-environment.yaml
	fi
done
}

################
# Creates all the desired vlans on the undercloud
vlanCreate ()
{
for (( i=0; i < ${#lines[@]}; i++))
do
	line=${lines[$i]}
	if [[ `echo $line | awk -F"," '{print $1}' | tr '[:upper:]' '[:lower:]'` == "vlans" ]]
	then
		vlanindex=$i
		break
	fi
done

for (( i=$vlanindex+1; i < ${#lines[@]}; i++))
do
	line=${lines[$i]}
	local vlanid=`echo $line | awk -F"," '{print $1}'`
	if [[ $vlanid =~ ^-?[0-9]+$ ]]
	then
		# this will create a file for the specified vlans
		echo "DEVICE=$uvlan.$vlanid" > undernet/ifcfg-$uvlan.$vlanid
		echo "BOOTPROTO=none" >> undernet/ifcfg-$uvlan.$vlanid
		echo "ONBOOT=yes" >> undernet/ifcfg-$uvlan.$vlanid
		echo "IPADDR=`echo $line | awk -F"," '{print $3}'`" >> undernet/ifcfg-$uvlan.$vlanid
		echo "NETMASK=255.255.255.0" >> undernet/ifcfg-$uvlan.$vlanid
		echo "NM_CONTROLLED=no" >> undernet/ifcfg-$uvlan.$vlanid
		echo "USERCTL=no" >> undernet/ifcfg-$uvlan.$vlanid
		echo "PREFIX=24" >> undernet/ifcfg-$uvlan.$vlanid
		echo "NETWORK=`echo $line | awk -F"," '{print $4}' | rev | cut -c 4- | rev`" >> undernet/ifcfg-$uvlan.$vlanid
		echo "VLAN=yes" >> undernet/ifcfg-$uvlan.$vlanid
	else
		if [[ ! $vlanid ]]
		then
			break
		fi
	fi
done
}

#################
# Creates a vm for the undercloud needs some work to figure out how to boot the physical hardware from a virtual node
vitualUndercloud()
{
# creates the stack user on the local machine if it doesn't exist
if [ ! -d "/home/stack/" ]
then
	useradd stack
fi

if [ ! -d "/home/stack/.ssh" ]
then
	mkdir /home/stack/.ssh/
	chown stack:stack /home/stack/.ssh/
fi

if [ ! -f "/home/stack/.ssh/authorized_keys" ]
then
	cp /root/.ssh/authorized_keys /home/stack/.ssh/
	chmod 600 /home/stack/.ssh/authorized_keys
	chown stack:stack /home/stack/.ssh/authorized_keys
fi

if [ ! -f "/etc/sudoers.d/stack" ]
then
	echo 'stack ALL=(root) NOPASSWD:ALL' >> /etc/sudoers.d/stack
	chmod 0440 /etc/sudoers.d/stack
fi

# check for and add if needed repositories required for installation
case $platform in
	Cen)
		yum -y install epel-release
		if ! [ -f /etc/yum.repos.d/delorean.repo ]
		then 
			curl -L -o /etc/yum.repos.d/delorean.repo http://buildlogs.centos.org/centos/7/cloud/x86_64/rdo-trunk-master-tripleo/delorean.repo
		fi

		if ! [ -f /etc/yum.repos.d/delorean-current.repo ]
		then 
			curl -L -o /etc/yum.repos.d/delorean-current.repo http://trunk.rdoproject.org/centos7/current/delorean.repo
			sed -i 's/\[delorean\]/\[delorean-current\]/' /etc/yum.repos.d/delorean-current.repo
			echo "includepkgs=diskimage-builder,instack,instack-undercloud,os-apply-config,os-cloud-config,os-collect-config,os-net-config,os-refresh-config,python-tripleoclient,tripleo-common,openstack-tripleo-heat-templates,openstack-tripleo-image-elements,openstack-tripleo,openstack-tripleo-puppet-elements,openstack-puppet-modules" >> /etc/yum.repos.d/delorean-current.repo
		fi

		if ! [ -f /etc/yum.repos.d/delorean-deps.repo ]
		then
			curl -L -o /etc/yum.repos.d/delorean-deps.repo http://trunk.rdoproject.org/centos7/delorean-deps.repo
		fi

		yum -y install yum-plugin-priorities
	;;
	Red)
		echo "need repos"
		#exit 1;
	;;
esac

yum install -y instack-undercloud

cp /root/*.* /home/stack/
chown stack:stack /home/stack/*.* 

sudo - stack
instack-virt-setup
echo "Undercloud VM setup complete please note ip of Virtual undercloud and run script again without 'virt'"
logout

exit 0;
}

#################
# Displays the instructions
instructions ()
{
echo "Incorrect argument. Please provide one of the following: Virt, Undercloud, Overcloud, Deploy Undercloud, Deploy Overcloud"
echo "Usage: ./Director_Deploy.sh undercloud or ./Director_Deploy.sh deploy undercloud" 
}

#################
# Prompts for the csv file name and checks if it's valid
getInfile ()
{
echo "Please provide the file with all the settings for the undercloud and overcloud" 
read -p "CSV Settings file: " infile

[ ! -f $infile ] && { echo "$infile file not found"; exit 99;}

}

#################
# Prompts for the Ip Address and checks if it can be accessed. Will prompt until the ip can be accessed.
getUnderIP ()
{
for (( i=2; i>1; i++)) 
do 
	read -p "ip address for Undercloud: " underip

	ssh -q root@$underip exit

	if [ $? -eq 0 ] 
	then
		echo 'Connected' 
		i=0 
	else
		echo 'Not Connected' 
		i=2
	fi	
done

# this will check if tunneling is being used
if [[ $underip == *"-p"* ]] || [[ $underip == *"-P"* ]] 
then
	tline=($underip)
	for (( i=0 ; i < ${#tline[@]} ; i++ ))
	do
		if [[ ${tline[$i]} == -p ]] || [[ ${tline[$i]} == -P ]]
		then
			port=${tline[$i+1]}
			if [ $i -eq 0 ]
			then
				underip=${tline[$i+2]}
			else
				underip=${tline[$i-1]}
			fi
		fi
	done
else
	port=22
fi

platform=$(getPlatform)

}

#################
# checks for Redhat or CentOS
getPlatform ()
{
# Gets the platform on the undercloud server Cen for CentOS and Red for Red Hat
echo `ssh -p $port root@$underip "cat /etc/redhat-release" |  cut -c -3`
}

################
# creates the undercloud.conf file needed for undercloud deployment
makeUnderConf ()
{

# gets the index for hte undercloud section
for (( i=0; i < ${#lines[@]}; i++))
do
	line=${lines[$i]}
	if [[ `echo $line | awk -F"," '{print $1}' | tr '[:upper:]' '[:lower:]'` == "undercloud" ]]
	then
		index=$i
		break
	fi
done

# Cycles throught the section and generates the file
for (( i=$index+1; i < ${#lines[@]}; i++ ))
do
	line=${lines[$i]}
	arg=`echo $line | awk -F"," '{print $2}' | tr '[:upper:]' '[:lower:]'`
	
	case $arg in

	#finds the vlan interface on undercloud
	vlan)
		uvlan=`echo $line | awk -F"," '{print $1}' | tr '[:upper:]' '[:lower:]'`
	;;

	provision)
		#make the undercloud.conf file
		ipbase=`echo $line | awk -F"," '{print $3}' | cut -d"." -f1-3`
		start=`echo $line | awk -F"," '{print $8}' | cut -d":" -f1`
		end=`echo $line | awk -F"," '{print $8}' | rev | cut -d":" -f1 | rev`
		ProHostIP=`echo $line | awk -F"," '{print $3}'`
		echo "[DEFAULT]" > undercloud.conf
		echo "local_ip = `echo $line | awk -F"," '{print $3}'`/`echo $line | awk -F"," '{print $4}' | awk -F '/' '{print $2}'`" >> undercloud.conf
		echo "image_path = /home/stack/images" >> undercloud.conf 
		echo "undercloud_hostname=`ssh -p $port root@$underip hostname`" >> undercloud.conf
		echo "network_gateway = `echo $line | awk -F"," '{print $7}'`" >> undercloud.conf
		echo "undercloud_public_vip = $ipbase.2" >> undercloud.conf
		echo "undercloud_admin_vip = $ipbase.3" >> undercloud.conf
		echo "local_interface = `echo $line | awk -F"," '{print $1}'`" >> undercloud.conf
		echo "network_cidr = `echo $line | awk -F"," '{print $4}'`" >> undercloud.conf
		echo "dhcp_start = `echo $line | awk -F"," '{print $5}'`" >> undercloud.conf
		echo "dhcp_end = `echo $line | awk -F"," '{print $6}'`" >> undercloud.conf
		echo "inspection_interface = br-ctlplane" >> undercloud.conf
		echo "inspection_iprange = $ipbase.$start,$ipbase.$end" >> undercloud.conf
		echo "inspection_extras = false" >> undercloud.conf
		echo "inspection_runbench = false" >> undercloud.conf
		echo "undercloud_debug = false" >> undercloud.conf
		echo "enable_tempest = true" >> undercloud.conf
		echo "ipxe_deploy = true" >> undercloud.conf
		echo "store_events = false" >> undercloud.conf
		echo "[auth]" >> undercloud.conf
	;;
	
	external)
		#[[ `uname` == "Darwin" ]] && sed -i .bk "s/.*local_interface.*/local_interface = `echo $line | awk -F"," '{print $1}'`/g" undercloud.conf
		#[[ `uname` == "Linux" ]] && sed -i "s/.*local_interface.*/local_interface = `echo $line | awk -F"," '{print $1}'`/g" undercloud.conf

		[[ -f undercloud.conf.bk ]] && rm undercloud.conf.bk
	;;

	*)
		break
	;;
	esac
	
done
}

################
# makes the network-environmental.yaml file
makeEnvYaml ()
{

# ensures the templates folder is created
[ ! -d templates ] && mkdir templates/

echo "resource_registry:" > templates/network-environment.yaml
echo "  OS::TripleO::Compute::Net::SoftwareConfig: /home/stack/templates/nic-configs/compute.yaml" >> templates/network-environment.yaml
echo "  OS::TripleO::Controller::Net::SoftwareConfig: /home/stack/templates/nic-configs/controller.yaml" >> templates/network-environment.yaml
echo "  OS::TripleO::CephStorage::Net::SoftwareConfig: /home/stack/templates/nic-configs/ceph-storage.yaml" >> templates/network-environment.yaml
echo "parameter_defaults:" >> templates/network-environment.yaml
echo "  ControlPlaneDefaultRoute: $ProHostIP" >> templates/network-environment.yaml
echo "  EC2MetadataIp: $ProHostIP" >> templates/network-environment.yaml
echo "  DnsServers: [\"8.8.8.8\",\"8.8.4.4\"]" >> templates/network-environment.yaml
echo "  NeutronExternalNetworkBridge: \"''\"" >> templates/network-environment.yaml

# cycles through the section setting up the output file
for (( i=$vlanindex+1; i < ${#lines[@]}; i++ ))
do
	line=${lines[$i]}	
	local vlanid=`echo $line | awk -F"," '{print $1}'`
	local traffic=`echo $line | awk -F"," '{print $2}' | tr '[:upper:]' '[:lower:]'` 
	case $traffic in
	external)
	# sets the external gateway for the external vlan in undercloud.conf if there is an external network instead of vlan this field is not needed 
		iprange=`echo $line | awk -F"," '{print $4}'`
		echo "masquerade_network = $iprange" >> undercloud.conf

	# inserts the external values for the templates/network-environment.yaml
		echo "  ExternalNetCidr: `echo $line | awk -F"," '{print $4}'`" >> templates/network-environment.yaml
		echo "  ExternalAllocationPools: [{'start': '`echo $line | awk -F"," '{print $5}'`', 'end': '`echo $line | awk -F"," '{print $6}'`'}]" >> templates/network-environment.yaml
		echo "  ExternalInterfaceDefaultRoute: `echo $line | awk -F"," '{print $7}'`" >> templates/network-environment.yaml
		echo "  ExternalNetworkVlanID: $vlanid" >> templates/network-environment.yaml
		vlanCreate $vlanid $uvlan $line
	;;
	internal)
	# inserts the internal values for the templates/network-environment.yaml
		echo "  InternalApiNetCidr: `echo $line | awk -F"," '{print $4}'`" >> templates/network-environment.yaml
		echo "  InternalApiAllocationPools: [{'start': '`echo $line | awk -F"," '{print $5}'`', 'end': '`echo $line | awk -F"," '{print $6}'`'}]" >> templates/network-environment.yaml
		echo "  InternalApiNetworkVlanID: $vlanid" >> templates/network-environment.yaml
	;;
	tenant)
	# inserts the tenant values for the templates/network-environment.yaml
		echo "  TenantNetCidr: `echo $line | awk -F"," '{print $4}'`" >> templates/network-environment.yaml
		echo "  TenantAllocationPools: [{'start': '`echo $line | awk -F"," '{print $5}'`', 'end': '`echo $line | awk -F"," '{print $6}'`'}]" >> templates/network-environment.yaml
		echo "  TenantNetworkVlanID: $vlanid" >> templates/network-environment.yaml
	;;
	storage)
	# inserts the storage values for the templates/network-environment.yaml
		echo "  StorageNetCidr: `echo $line | awk -F"," '{print $4}'`" >> templates/network-environment.yaml
		echo "  StorageAllocationPools: [{'start': '`echo $line | awk -F"," '{print $5}'`', 'end': '`echo $line | awk -F"," '{print $6}'`'}]" >> templates/network-environment.yaml
		echo "  StorageNetworkVlanID: $vlanid" >> templates/network-environment.yaml
	;;
	storagemgmt)
	# inserts the storage management values for the templates/network-environment.yaml
		echo "  StorageMgmtNetCidr: `echo $line | awk -F"," '{print $4}'`" >> templates/network-environment.yaml
		echo "  StorageMgmtAllocationPools: [{'start': '`echo $line | awk -F"," '{print $5}'`', 'end': '`echo $line | awk -F"," '{print $6}'`'}]" >> templates/network-environment.yaml
		echo "  StorageMgmtNetworkVlanID: $vlanid" >> templates/network-environment.yaml
	;;
	management)
	# inserts the management values for the templates/network-environment.yaml
		echo "  ManagementNetCidr: `echo $line | awk -F"," '{print $4}'`" >> templates/network-environment.yaml
		echo "  ManagementAllocationPools: [{'start': '`echo $line | awk -F"," '{print $5}'`', 'end': '`echo $line | awk -F"," '{print $6}'`'}]" >> templates/network-environment.yaml
		echo "  ManagementNetworkVlanID: $vlanid" >> templates/network-environment.yaml
	;;
	*)
		if [[ ! $traffic ]]
		then
			break
		fi
	esac
done

}

###############
# this basically saves the overcloud provisioning and vlan networks to variables
overcloud ()
{
for (( i=0; i < ${#lines[@]}; i++))
do
	line=${lines[$i]}
	if [[ `echo $line | awk -F"," '{print $1}' | tr '[:upper:]' '[:lower:]'` == "overcloud" ]]
	then
		overindex=$i
		break
	fi
done

for (( i=$overindex+1; i < ${#lines[@]}; i++))
do
	line=${lines[$i]}
	case `echo $line | awk -F"," '{print $2}' | tr '[:upper:]' '[:lower:]'` in 
	provision)
		opronet=`echo $line | awk -F"," '{print $1}' | tr '[:upper:]' '[:lower:]'`
	;;
	vlan)
		ovlannet=`echo $line | awk -F"," '{print $1}' | tr '[:upper:]' '[:lower:]'`
	;;
	*)
		if [[ ! `echo $line | awk -F"," '{print $2}' | tr '[:upper:]' '[:lower:]'` ]]
		then
			break
		fi
	;;
	esac
done
}

################
# This makes the instack.json file for introspection
makeInstJson ()
{
# saves the index for the nodes section
for (( i=0; i < ${#lines[@]}; i++))
do
	line=${lines[$i]}
	if [[ `echo $line | awk -F"," '{print $1}' | tr '[:upper:]' '[:lower:]'` == "nodes" ]]
	then
		nodesindex=$i
		break
	fi
done

int=0

for (( i=$nodesindex+1; i < ${#lines[@]}; i++))
do
	line=${lines[$i]}
# increment counter for each type of node for deployment of overcloud
	case `echo $line | awk -F"," '{print $10}' | tr '[:upper:]' '[:lower:]' | head -c4` in
	cont)
		((control++))
	;;
	comp)
		((compute++))
	;;
	ceph)
		((ceph++))
	;;
	*)
		if [[ ! `echo $line | awk -F"," '{print $2}' | tr '[:upper:]' '[:lower:]'` ]]
		then
			break
		fi
	;;
	esac

# Make instackenv.json to prepeare for node introspection 		
	case $int in
	0)
# this increments the counter and goes to the next iteration if the first line is the title bar 
		((int++))
		[[ `echo $line | awk -F"," '{print $1}'` == "Name" ]] && continue
	;;
	1)
#This will insert the first item into the list fields are only populated if they are included in the csv
		echo "{" > instackenv.json
		[ ! -z `echo $line | awk -F"," '{print $12}'` ] && echo "  \"ssh-user\": \"`echo $line | awk -F"," '{print $12}'`\"," >> instackenv.json
		[ ! -z `echo $line | awk -F"," '{print $13}'` ] && echo "  \"ssh-key\": \"ssh-rsa `echo $line | awk -F"," '{print $13}'`\"," >> instackenv.json
		[ ! -z `echo $line | awk -F"," '{print $14}'` ] && echo "  \"power-manager\": \"`echo $line | awk -F"," '{print $14}'`\"," >> instackenv.json
		[ ! -z `echo $line | awk -F"," '{print $12}'` ] && echo "  \"host-ip\": \"`echo $line | awk -F"," '{print $6}'`\"," >> instackenv.json
		echo "  \"nodes\": [" >> instackenv.json
		echo "    {" >> instackenv.json
		echo "      \"mac\": [" >> instackenv.json
		[ ! -z `echo $line | awk -F"," '{print $2}'` ] && echo "        \"`echo $line | awk -F"," '{print $2}'`\"" >> instackenv.json
		echo "      ]," >> instackenv.json
		[ ! -z `echo $line | awk -F"," '{print $1}'` ] && echo "      \"name\":\"`echo $line | awk -F"," '{print $1}'`\"," >> instackenv.json
		[ ! -z `echo $line | awk -F"," '{print $3}'` ] && echo "      \"cpu\":\"`echo $line | awk -F"," '{print $3}'`\"," >> instackenv.json
		[ ! -z `echo $line | awk -F"," '{print $4}'` ] && echo "      \"memory\":\"`echo $line | awk -F"," '{print $4}'`\"," >> instackenv.json
		[ ! -z `echo $line | awk -F"," '{print $5}'` ] && echo "      \"disk\":\"`echo $line | awk -F"," '{print $5}'`\"," >> instackenv.json
		[ ! -z `echo $line | awk -F"," '{print $6}'` ] && echo "      \"pm_addr\":\"`echo $line | awk -F"," '{print $6}'`\"," >> instackenv.json
		[ ! -z `echo $line | awk -F"," '{print $7}'` ] && echo "      \"pm_password\":\"`echo $line | awk -F"," '{print $7}'`\"," >> instackenv.json
		[ ! -z `echo $line | awk -F"," '{print $8}'` ] && echo "      \"pm_type\":\"`echo $line | awk -F"," '{print $8}'`\"," >> instackenv.json
		[ ! -z `echo $line | awk -F"," '{print $9}'` ] && echo "      \"pm_user\":\"`echo $line | awk -F"," '{print $9}'`\"," >> instackenv.json
		[ ! -z `echo $line | awk -F"," '{print $10}'` ] && echo "      \"capabilities\":\"profile:`echo $line | awk -F"," '{print $10}'`,boot_option:local\"" >> instackenv.json
		((int++))
		continue
	;;
	*)
# all other nodes are added the same way
		if [ `echo $line | awk -F"," '{print $2}'` ]
		then
			echo "    }," >> instackenv.json
			echo "    {" >> instackenv.json
			[ ! -z `echo $line | awk -F"," '{print $12}'` ] && echo "  \"ssh-user\": \"`echo $line | awk -F"," '{print $12}'`\"," >> instackenv.json
			[ ! -z `echo $line | awk -F"," '{print $13}'` ] && echo "  \"ssh-key\": \"`echo $line | awk -F"," '{print $13}'`\"," >> instackenv.json
			[ ! -z `echo $line | awk -F"," '{print $14}'` ] && echo "  \"power-manager\": \"`echo $line | awk -F"," '{print $14}'`\"," >> instackenv.json
			[ ! -z `echo $line | awk -F"," '{print $12}'` ] && echo "  \"host-ip\": \"`echo $line | awk -F"," '{print $6}'`\"," >> instackenv.json
			echo "      \"mac\": [" >> instackenv.json
			[ ! -z `echo $line | awk -F"," '{print $2}'` ] && echo "        \"`echo $line | awk -F"," '{print $2}'`\"" >> instackenv.json
			echo "      ]," >> instackenv.json
			[ ! -z `echo $line | awk -F"," '{print $1}'` ] && echo "      \"name\":\"`echo $line | awk -F"," '{print $1}'`\"," >> instackenv.json
			[ ! -z `echo $line | awk -F"," '{print $3}'` ] && echo "      \"cpu\":\"`echo $line | awk -F"," '{print $3}'`\"," >> instackenv.json
			[ ! -z `echo $line | awk -F"," '{print $4}'` ] && echo "      \"memory\":\"`echo $line | awk -F"," '{print $4}'`\"," >> instackenv.json
			[ ! -z `echo $line | awk -F"," '{print $5}'` ] && echo "      \"disk\":\"`echo $line | awk -F"," '{print $5}'`\"," >> instackenv.json
			[ ! -z `echo $line | awk -F"," '{print $6}'` ] && echo "      \"pm_addr\":\"`echo $line | awk -F"," '{print $6}'`\"," >> instackenv.json
			[ ! -z `echo $line | awk -F"," '{print $7}'` ] && echo "      \"pm_password\":\"`echo $line | awk -F"," '{print $7}'`\"," >> instackenv.json
			[ ! -z `echo $line | awk -F"," '{print $8}'` ] && echo "      \"pm_type\":\"`echo $line | awk -F"," '{print $8}'`\"," >> instackenv.json
			[ ! -z `echo $line | awk -F"," '{print $9}'` ] && echo "      \"pm_user\":\"`echo $line | awk -F"," '{print $9}'`\"," >> instackenv.json
			[ ! -z `echo $line | awk -F"," '{print $10}'` ] && echo "      \"capabilities\":\"profile:`echo $line | awk -F"," '{print $10}'`,boot_option:local\"" >> instackenv.json
		fi

# this will close the list once the next line is not a node
		if [ ! `echo ${lines[$i+1]} | awk -F"," '{print $2}'` ] 
		then
		# closes the file when the next section is coming
			echo "    }" >> instackenv.json	
			echo "  ]" >> instackenv.json
			echo "}" >> instackenv.json
			break
		fi		
	;;
	esac
done
}

################
#Installs the undercloud on the remote ip
instUndercloud ()
{

# creates the stack user if it doesn't exist
if ssh -p $port root@$underip [ ! -d "/home/stack/" ]
then
	ssh -p $port root@$underip "useradd stack"
fi

if ssh -p $port root@$underip [ ! -d "/home/stack/.ssh" ]
then
	ssh -p $port root@$underip "mkdir /home/stack/.ssh/"
	ssh -p $port root@$underip "chown stack:stack /home/stack/.ssh/"
fi

if ssh -p $port root@$underip [ ! -f "/home/stack/.ssh/authorized_keys" ]
then
	ssh -p $port root@$underip "cp /root/.ssh/authorized_keys /home/stack/.ssh/"
	ssh -p $port root@$underip "chmod 600 /home/stack/.ssh/authorized_keys"
	ssh -p $port root@$underip "chown stack:stack /home/stack/.ssh/authorized_keys"
fi

if ssh -p $port root@$underip [ ! -f "/etc/sudoers.d/stack" ]
then
	ssh -p $port root@$underip "echo 'stack ALL=(root) NOPASSWD:ALL' >> /etc/sudoers.d/stack"
	ssh -p $port root@$underip "chmod 0440 /etc/sudoers.d/stack"
fi

# Sets the hostname on undercloud
#hostf=`ssh -p $port root@$underip hostname -A`
#hosts=`ssh -p $port root@$underip hostname -s`

#if ! ssh -p $port root@$underip "grep \"$hostf $hosts\" \"/etc/hosts\" | grep -q 127.0.0.1"
#then
#	ssh -p $port root@$underip "sed -i \"/127.0.0.1/ s/$/ $hostf $hosts/\" /etc/hosts"
#fi

# create directories for deployment files on the remote system
ssh -p $port stack@$underip "[ ! -d /home/stack/images ] && mkdir /home/stack/images"
ssh -p $port stack@$underip "[ ! -d /home/stack/templates ] && mkdir /home/stack/templates"

# check for and add if needed repositories required for installation
case $platform in
	Cen)
		ssh -p $port root@$underip yum -y install epel-release
		case $variable3 in 
		mitaka)	
			echo "Installing Mitaka repos"	
			if ! ssh -p $port root@$underip "[ -f /etc/yum.repos.d/delorean-mitaka.repo ]"
			then 
				ssh -p $port root@$underip "curl -L -o /etc/yum.repos.d/delorean-mitaka.repo https://trunk.rdoproject.org/centos7-mitaka/current/delorean.repo"
				ssh -p $port root@$underip "sed -i 's/\[delorean\]/\[delorean-mitaka\]/' /etc/yum.repos.d/delorean-mitaka.repo"
			fi

			if ! ssh -p $port root@$underip "[ -f /etc/yum.repos.d/delorean-deps-mitaka.repo ]"
			then 
				ssh -p $port root@$underip "curl -L -o /etc/yum.repos.d/delorean-deps-mitaka.repo http://trunk.rdoproject.org/centos7-mitaka/delorean-deps.repo"
				ssh -p $port root@$underip "sed -i 's/\[delorean\]/\[delorean-deps-mitaka\]/' /etc/yum.repos.d/delorean-deps-mitaka.repo"
			fi

			ssh -p $port root@$underip yum -y install --enablerepo=extras centos-release-ceph-hammer
			ssh -p $port root@$underip sed -i -e 's%gpgcheck=.*%gpgcheck=0%' /etc/yum.repos.d/CentOS-Ceph-Hammer.repo
		;;

		*)
			if ! ssh -p $port root@$underip "[ -f /etc/yum.repos.d/delorean.repo ]"
			then 
				ssh -p $port root@$underip "curl -L -o /etc/yum.repos.d/delorean.repo http://buildlogs.centos.org/centos/7/cloud/x86_64/rdo-trunk-master-tripleo/delorean.repo"
			fi

			if ! ssh -p $port root@$underip "[ -f /etc/yum.repos.d/delorean-current.repo ]"
			then 
				ssh -p $port root@$underip "curl -L -o /etc/yum.repos.d/delorean-current.repo http://trunk.rdoproject.org/centos7/current/delorean.repo"
				ssh -p $port root@$underip "sed -i 's/\[delorean\]/\[delorean-current\]/' /etc/yum.repos.d/delorean-current.repo"
				ssh -p $port root@$underip "printf \"\nincludepkgs=diskimage-builder,instack,instack-undercloud,os-apply-config,os-cloud-config,os-collect-config,os-net-config,os-refresh-config,python-tripleoclient,tripleo-common,openstack-tripleo-heat-templates,openstack-tripleo-image-elements,openstack-tripleo,openstack-tripleo-puppet-elements,openstack-puppet-modules\" >> /etc/yum.repos.d/delorean-current.repo"
			fi

			if ! ssh -p $port root@$underip "[ -f /etc/yum.repos.d/delorean-deps.repo ]"
			then
				ssh -p $port root@$underip "curl -L -o /etc/yum.repos.d/delorean-deps.repo http://trunk.rdoproject.org/centos7/delorean-deps.repo"
			fi

			ssh -p $port root@$underip yum install yum-utils -y
			ssh -p $port root@$underip yum-config-manager --add-repo https://raw.githubusercontent.com/CentOS-Storage-SIG/centos-release-ceph-jewel/master/CentOS-Ceph-Jewel.repo
			ssh -p $port root@$underip yum-config-manager --disable centos-ceph-jewel
			ssh -p $port root@$underip yum-config-manager --enable centos-ceph-jewel-test
			ssh -p $port root@$underip sed -i -e 's%gpgcheck=.*%gpgcheck=0%' /etc/yum.repos.d/CentOS-Ceph-Jewel.repo
		;;
		esac

		ssh -p $port root@$underip yum -y install yum-plugin-priorities
	;;
	Red)
		ssh -p $port root@$underip "yum install -y yum-utils"
		ssh -p $port root@$underip "yum-config-manager --enable rhelosp-rhel-7-server-opt"

		ssh -p $port root@$underip "yum -y install epel-release"
		if ! ssh -p $port root@$underip "[ -f /etc/yum.repos.d/delorean.repo ]"
		then 
			ssh -p $port root@$underip "curl -L -o /etc/yum.repos.d/delorean.repo http://buildlogs.centos.org/centos/7/cloud/x86_64/rdo-trunk-master-tripleo/delorean.repo"
		fi

		if ! ssh -p $port root@$underip "[ -f /etc/yum.repos.d/delorean-current.repo ]"
		then 
			ssh -p $port root@$underip "curl -L -o /etc/yum.repos.d/delorean-current.repo http://trunk.rdoproject.org/centos7/current/delorean.repo"
			ssh -p $port root@$underip "sed -i 's/\[delorean\]/\[delorean-current\]/' /etc/yum.repos.d/delorean-current.repo"
			ssh -p $port root@$underip "printf \"\nincludepkgs=diskimage-builder,instack,instack-undercloud,os-apply-config,os-cloud-config,os-collect-config,os-net-config,os-refresh-config,python-tripleoclient,tripleo-common,openstack-tripleo-heat-templates,openstack-tripleo-image-elements,openstack-tripleo,openstack-tripleo-puppet-elements,openstack-puppet-modules\" >> /etc/yum.repos.d/delorean-current.repo"
		fi
		if ! ssh -p $port root@$underip "[ -f /etc/yum.repos.d/delorean-deps.repo ]"
		then
			ssh -p $port root@$underip "curl -L -o /etc/yum.repos.d/delorean-deps.repo http://trunk.rdoproject.org/centos7/delorean-deps.repo"
		fi
		ssh -p $port root@$underip yum -y install yum-plugin-priorities
	;;
esac

# Replaces the network files on the undercloud with the ones the script made. Checks to make sure undercloud has CentOS or Red Hat
case $platform in
	Cen|Red)
		echo "Setting up VLans"
		scp -P $port undernet/* root@$underip:/etc/sysconfig/network-scripts/
		ssh -p $port root@$underip systemctl stop NetworkManager
		ssh -p $port root@$underip systemctl disable NetworkManager
		ssh -p $port root@$underip systemctl restart network
		ssh -p $port root@$underip "systemctl enable network"
	;;
	*)
# only centOS and Red Hat are valid OSs
		echo "No Valid OS found"
		exit 1;
	;;
esac

# install tripleo on undercloud
ssh -p $port root@$underip "yum install -y python-tripleoclient"

# checks to see if trippleo is installed
if ! ssh -p $port root@$underip "rpm -qa | grep -qw python-tripleoclient" 
then
	echo "tripleo did not install"
	exit 2;
fi

# copies the configuration for the undercloud
scp -P $port undercloud.conf stack@$underip:/home/stack/

# undercloud install
ssh -p $port -t -t stack@$underip "export DIB_INSTALLTYPE_puppet_modules=source; openstack undercloud install" 2> /dev/null
}

################
# creates a script for the deployment of the overcloud. This is saved on the undercloud. 
makeOverSh ()
{
# create overcloud.sh with information for future updates
cat > overcloud.sh << 'EOF'
#!/bin/bash
source /home/stack/stackrc

openstack overcloud deploy \
--templates \
EOF

if [[ $ovlannet ]]
then
	cat >> overcloud.sh << EOF
  -e /usr/share/openstack-tripleo-heat-templates/environments/network-isolation.yaml \\
  -e /home/stack/templates/network-environment.yaml \\
EOF
fi

cat >> overcloud.sh << EOF
  -e /home/stack/templates/storage-environment.yaml \\
  -e /home/stack/templates/firstboot/wipe-disks.yaml \\
--control-flavor control \\
--compute-flavor compute \\
--ceph-storage-flavor ceph-storage \\
--control-scale $control \\
--compute-scale $compute \\
--ceph-storage-scale $ceph \\
--ntp-server pool.ntp.org \\
--neutron-network-type vxlan \\
--neutron-tunnel-types vxlan
EOF

chmod 755 overcloud.sh

}

################
# this performs tasks after the undercloud is deployed
postUndercloud ()
{
# Check to make sure ironic installed carrectly before 
[[ ! `ssh -p $port root@$underip "rpm -qa | grep -w python-ironic-inspector"` ]] && echo "Ironic-inspector did not install" && exit 3

# install images for ironic
case $platform in 
	Cen)
		cat > image-create.sh << 'EOF'
#!/bin/bash
export NODE_DIST=centos7
export USE_DELOREAN_TRUNK=1
export DELOREAN_TRUNK_REPO="http://buildlogs.centos.org/centos/7/cloud/x86_64/rdo-trunk-master-tripleo/"
export DELOREAN_REPO_FILE="delorean.repo"
source /home/stack/stackrc
openstack overcloud image build --all
EOF
		chmod 755 image-create.sh
		scp -P $port image-create.sh stack@$underip:/home/stack
		ssh -p $port -t -t stack@$underip "/home/stack/image-create.sh" 2> /dev/null

		if ssh -p $port stack@$underip [ -f /home/stack/ironic-python-agent.initramfs ]
		then
			ssh -p $port -t -t stack@$underip "source /home/stack/stackrc; openstack overcloud image upload" 2> /dev/null
		else
			echo "Images were not build correctly"
			exit 4;
		fi
	;;
	Red)
		ssh -p $port root@$underip "yum install -y rhosp-director-images rhosp-director-images-ipa"

		for i in `ssh -p $port root@$underip "ls /usr/share/rhosp-director-images/overcloud-full-*"`
		do
			ssh -p $port root@$underip "cp $i /home/stack/images/."
		done

		for i in `ssh -p $port root@$underip "ls /usr/share/rhosp-director-images/ironic-python-agent-*"`
		do
			ssh -p $port root@$underip "cp $i /home/stack/images/."
		done

		for tarfile in `ssh -p $port root@$underip "ls /home/stack/images/*.tar"`
		do 
			ssh -p $port root@$underip "tar -xf $tarfile -C /home/stack/images/"
		done

		ssh -p $port -t -t stack@$underip "source /home/stack/stackrc; openstack overcloud image upload --image-path /home/stack/images/" 2> /dev/null
	;;
esac

# Sets the DNS for the undercloud
local sid=$(ssh -p $port -t -t stack@$underip "source /home/stack/stackrc; neutron subnet-list | grep start" | awk '{print $2}')
local dns=$(ssh -p $port -t -t stack@$underip "source /home/stack/stackrc; neutron subnet-show $sid | grep gateway_ip" | awk '{print $4}')
ssh -p $port -t -t stack@$underip "source /home/stack/stackrc; neutron subnet-update $sid --dns-nameserver $dns" 2> /dev/null

# Copies the instackenv.json file in preparation for introspection
scp -P $port instackenv.json stack@$underip:/home/stack/

# imports the values from the instackenv.json file and prepares for introspection
ssh -p $port -t -t stack@$underip "source /home/stack/stackrc; openstack baremetal import --json /home/stack/instackenv.json" 2> /dev/null
ssh -p $port -t -t stack@$underip "source /home/stack/stackrc; openstack baremetal configure boot" 2> /dev/null

#makes sure all nodes are off
for node in $(ssh -p $port -t -t stack@$underip "source /home/stack/stackrc; ironic node-list | grep power\ on" | awk '{print $2}')
do 
	ssh -p $port -t -t stack@$underip "source /home/stack/stackrc; ironic node-set-power-state $node off" 2> /dev/null
done

#set nodes to manageable
for node in $(ssh -p $port -t -t stack@$underip "source /home/stack/stackrc; ironic node-list | grep available" | awk '{print $2}')
do 
	ssh -p $port -t -t stack@$underip "source /home/stack/stackrc; ironic node-set-provision-state $node manage" 2> /dev/null
done

# introspection
case $varaible3 in
mitaka)
	ssh -p $port -t -t stack@$underip "source /home/stack/stackrc; openstack baremetal introspection bulk start"
;;
*)
	ssh -p $port -t -t stack@$underip "source /home/stack/stackrc; openstack overcloud node introspect --all-manageable" 2> /dev/null
;;
esac

# makes sure that the nodes are not in maneageable state after introspection
for node in $(ssh -p $port -t -t stack@$underip "source /home/stack/stackrc; ironic node-list | grep manageable" | awk '{print $2}')
do 
	ssh -p $port -t -t stack@$underip "source /home/stack/stackrc; ironic node-set-provision-state $node provide" 2> /dev/null
done

# set root Disk serial and check root drive size
for (( i=$nodesindex ; i < ${#lines[@]} ; i++ ))
do
	local end=$i
	[ -z `echo ${lines[$i+1]} | awk -F"," '{print $2}' | tr -d '[[:space:]]'` ] && break
done

oldIFS=$IFS
IFS=$'\n'
for i in `ssh -p $port -t -t stack@$underip "source /home/stack/stackrc; ironic node-list" 2> /dev/null | grep available`
do
	for (( j=$nodesindex ; j<=$end; j++ ))
	do
		line=${lines[$j]}
		if [ `echo $line | awk -F"," '{print $1}'` == `echo $i | awk '{print $4}'` ]
		then 
			node=`echo $i | awk '{print $2}'`
			drive_gb=`ssh -p $port -t -t stack@$underip "source /home/stack/stackrc; ironic node-show $node --fields properties" 2> /dev/null | tr "," "\n" | grep local_gb | awk -F":" '{print $2}' | tr "\'" "\n" | egrep -v "u|}"`
			wait
			root_gb=`echo $line | awk -F"," '{print $5}'`

			if [ "$drive_gb" != "$root_gb" ]
			then
				ssh -p $port -t -t stack@$underip "source /home/stack/stackrc; ironic node-update $node replace properties/local_gb=$root_gb" 2> /dev/null
			fi

			if [[ `echo $line | awk -F"," '{print $11}'` ]]
			then
				ssh -p $port -t -t stack@$underip "source /home/stack/stackrc; ironic node-update $node add properties/root_device=\"{\"serial\":\"`echo $line | awk -F"," '{print $11}'`\"}\"" 2> /dev/null
			fi
			break
		fi
	done
done
IFS=$oldIFS
}


####### BEGIN SCRIPT BODY #######
################
# Path

export PATH=/usr/kerberos/sbin:/usr/kerberos/bin:/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin

# Security
umask 022


####### Variables Begin #######
# these will keep a count of the number of nodes for the overcloud 
compute=0
control=0
ceph=0


if [[ $# -gt 0 ]] 
then
	variable=$( echo $1 | tr '[:upper:]' '[:lower:]' )
	case $variable in 
	virt)
		echo "A VM will be created for the undercloud, please note the IP at the end of the creation"
		vitualUndercloud
		exit 0;
	;;
	undercloud)
		echo "All the necessary files for the Undercloud will be created. However the undercloud will not be deployed"
		getInfile
		getUnderIP

		#reads the csv and places it into an array
		IFS=$'\n\r' read -d '' -r -a lines < $infile

		echo "Creating some directories"
		#creates directories for our files to go
		[ ! -d undernet ] && mkdir undernet

		echo "Creating undercloud.conf"
		makeUnderConf

		echo "Creating Vlans files"
		vlanCreate

		echo "Creating instackenv.json"
		makeInstJson

		echo "Files are located in current directory ./ and in ./undernet/" 

		exit 0;
	;;

	overcloud)
		echo "All the necessary files for the Undercloud and Overcloud will be created. However neither the Undercloud nor the Overcloud will be deployed"
		getInfile
		getUnderIP

		#reads the csv and places it into an array
		IFS=$'\n\r' read -d '' -r -a lines < $infile

		echo "Creating some directories"
		#creates directories for our files to go
		[ ! -d undernet ] && mkdir undernet

		echo "Creating undercloud.conf"
		makeUnderConf

		echo "Creating Vlans files"
		vlanCreate

		overcloud

		if [[ $ovlannet ]]
		then
			echo "Creating network_environmental.yaml"
			makeEnvYaml

			echo "Creating controller.yaml"
			makeContYaml

			echo "Creating compute.yaml"
			makeCompYaml

			echo "Creating ceph-storage.yaml"
			makeCephYaml
		fi

		echo "Creating files to setup Ceph"
		storage 

		echo "Creating instackenv.json"
		makeInstJson
		
		echo "creating Overcloud.sh"
		makeOverSh

		exit 0;
	;;

	deploy)
		variable2=$( echo $2 | tr '[:upper:]' '[:lower:]' )

		if [[ $3 ]]
		then
			variable3=$( echo $3 | tr '[:upper:]' '[:lower:]' )
		fi

		case $variable2 in
		undercloud)
			echo "This will generate required files for the Undercloud and deploy it"
			getInfile
			getUnderIP

			#reads the csv and places it into an array
			IFS=$'\n\r' read -d '' -r -a lines < $infile

			echo "Creating some directories"
			#creates directories for our files to go into
			[ ! -d undernet ] && mkdir undernet

			echo "Creating undercloud.conf" 
			makeUnderConf

			echo "Creating Vlans files"
			vlanCreate

			echo "installing undecloud"
			instUndercloud

			echo "Creating instackenv.json"
			makeInstJson

			echo "Running post undercloud installation"
			postUndercloud

			echo "Undercloud has been installed. Please check nodes using \"ironic node-list\""
			exit 0;
		;;

		overcloud)
			echo "This will generate all required files and deploy both the undercloud and overcloud"
			getInfile
			getUnderIP

			#reads the csv and places it into an array
			IFS=$'\n\r' read -d '' -r -a lines < $infile

			echo "Creating some directories"
			#creates directories for our files to go
			[ ! -d undernet ] && mkdir undernet
			[ ! -d templates ] && mkdir templates

			echo "Creating undercloud.conf"
			makeUnderConf

			echo "Creating Vlans"
			vlanCreate

			overcloud

			if [[ $ovlannet ]]
			then
				echo "Creating network_environmental.yaml"
				makeEnvYaml

				echo "Creating controller.yaml"
				makeContYaml
	
				echo "Creating compute.yaml"
				makeCompYaml

				echo "Creating ceph-storage.yaml"
				makeCephYaml

				# copy a few other files over to templates/nic-configs
				ssh -p $port stack@$underip cp /usr/share/openstack-tripleo-heat-templates/network/config/single-nic-vlans/controller-*.yaml /home/stack/templates/nic-configs/
				ssh -p $port stack@$underip cp /usr/share/openstack-tripleo-heat-templates/network/config/single-nic-vlans/swift-storage.yaml /home/stack/templates/nic-configs/
				ssh -p $port stack@$underip cp /usr/share/openstack-tripleo-heat-templates/network/config/single-nic-vlans/cinder-storage.yaml /home/stack/templates/nic-configs/
			fi

			echo "Creating files to setup Ceph"
			storage 

			echo "Creating instackenv.json"
			makeInstJson

			if [[ ! `ssh -p $port root@$underip "rpm -qa | grep -w python-ironic-inspector"` ]]
			then
				echo "Installing Undercloud"
				instUndercloud
				echo "Running post undercloud installation"
				postUndercloud
			else
				echo "The undercloud is installed." 
				read -p "Would you like to reinstall it? <y/N> " response

				case $response in
				[yY][eE][sS]|[yY])
					instUndercloud
					echo "Running post undercloud installation"
					postUndercloud
				;;
				esac	
			fi



			# move environmental file 
			scp -P $port -rp templates/ stack@$underip:/home/stack/

			echo "Creating Overcloud.sh"
			makeOverSh

			# move it to undercloud
			scp -P $port overcloud.sh stack@$underip:.

			echo "Instaling Overcloud"
			#ssh -p $port -t -t stack@$underip /home/stack/overcloud.sh

			exit 0;
		;;

		*)
			instructions
			exit 0;
		;;
		esac
	;;

	*)
		instructions 
		exit 0;
	;;
	esac
fi

instructions
exit 0;


