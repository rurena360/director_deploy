#!/bin/bash

echo "Please provide the ip of the machine you would like to get the hard drive serial number for"
read -p "Host IP: " hostip

[[ ! `ssh root@$hostip "rpm -qa | grep hdparm"` ]] && ssh root@$hostip "yum install -y hdparm" > /dev/null 2>&1

echo "The root disk Serial Number is: `ssh root@$hostip "hdparm -I /dev/sda | grep 'Serial\ Number'" | awk '{print $3}'`"
